# MTCNN-EyesRatio
Use MTCNN to calculate eyes open ratio

## How-to
1. download alignment dataset from [here](https://raw.githubusercontent.com/AKSHAYUBHAT/TensorFace/master/openface/models/dlib/shape_predictor_68_face_landmarks.dat) and put in the same directory
2. run live.py

## Results
![alt text](train_models/1.png)
![alt text](train_models/2.png)

